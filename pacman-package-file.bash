#!/usr/bin/env bash
# NAME
#        pacman-package-file.bash - Get original file contents from package
#
# SYNOPSIS
#        pacman-package-file.bash PATH
#        pacman-package-file.bash --help
#
# DESCRIPTION
#        Prints the contents of the given path as it is in the original package.
#        Please be aware that this may not be the same contents as the original
#        file as *installed,* since the package might modify the file during
#        installation.
#
#        Based on pkg-extract_original
#        <https://xyne.archlinux.ca/projects/pkg_scripts/#help-message-pkg-extract_original>.
#
# EXAMPLES
#        pacman-package-file.bash /etc/sudoers
#               Prints the contents of /etc/sudoers in the "sudo" package.
#
#        pacman-package-file.bash /etc/pacman.conf
#               Prints the contents of /etc/pacman.conf in the "pacman" package.
#
# EXIT STATUS
#        0: Success
#        1: Unknown error
#
#        Skipping systemd exit codes
#        <https://freedesktop.org/software/systemd/man/systemd.exec.html#Process%20exit%20codes>:
#        8: Owning package is ambiguous
#        9: Package file does not exist
#
#        Standard exit codes from /usr/include/sysexits.h:
#        64: Command line usage error
#
# BUGS
#        https://gitlab.com/victor-engmark/pacman-package-file/-/issues
#
# COPYRIGHT AND LICENSE
#        Copyright (C) 2020 Victor Engmark
#
#        This program is free software: you can redistribute it and/or modify
#        it under the terms of the GNU Affero General Public License as
#        published by the Free Software Foundation, either version 3 of the
#        License, or (at your option) any later version.
#
#        This program is distributed in the hope that it will be useful,
#        but WITHOUT ANY WARRANTY; without even the implied warranty of
#        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#        GNU Affero General Public License for more details.
#
#        You should have received a copy of the GNU Affero General Public
#        License along with this program.  If not, see
#        <https://www.gnu.org/licenses/>.
################################################################################

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob

usage() {
    while IFS= read -r line
    do
        case "$line" in
            '#!'*) # Shebang line
                ;;
            '##'*) # End of comments
                return
                ;;
            *) # Comment line
                printf '%s\n' "${line:2}" # Remove comment prefix
                ;;
        esac
    done < "$0"
}

if [[ "$#" -eq 0 ]]
then
    usage >&2
    exit 64
fi

if [[ "$1" = '--help' ]]
then
    usage
    exit
fi

cleanup() {
    if [[ -e "$working_directory" ]]
    then
        rm --recursive "$working_directory"
    fi
}
trap cleanup EXIT
working_directory="$(mktemp --directory)"

script_name="$(basename "$0")"

# Add extra pacman parameters if specified
pacman_command=('pacman')
if [[ -n "${PACMAN_CONFIG-}" ]]
then
    pacman_command+=("--config=${PACMAN_CONFIG}")
fi

owners_stderr="${working_directory}/owners_stderr"
mapfile -t owners < <("${pacman_command[@]}" --query --owns --quiet "$1" 2> "$owners_stderr")

if [[ -s "$owners_stderr" ]]
then
    sed -e "s/^error:/${script_name}:/" < "$owners_stderr" >&2
    exit 1
fi

if [[ "${#owners[@]}" -gt 1 ]]
then
    echo "${script_name}: Multiple packages own ${1}" >&2
    exit 8
fi

owner="${owners[0]}"
package_url="$("${pacman_command[@]}" --sync --print "$owner")"
if [[ "${package_url::7}" != 'file://' ]]
then
    printf "%s: Package missing - download using \`pacman --sync --downloadonly %s\` and retry\n" "$script_name" "$owner" >&2
    exit 9
fi

tar --extract --file "${package_url:7}" --to-stdout "${1#/}"
