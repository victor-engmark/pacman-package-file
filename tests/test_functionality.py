import re
from os.path import dirname
from subprocess import PIPE, run
from tempfile import NamedTemporaryFile

DIRECTORY = dirname(__file__)
DOCUMENTATION_REGEX = re.compile(
    rb"""
    NAME\n
    .+
    \n
    SYNOPSIS\n
    .+
    \n
    DESCRIPTION\n
    .+
    \n
    EXAMPLES\n
    .+
    \n
    EXIT[ ]STATUS\n
    .+
    \n
    BUGS\n
    .+
    \n
    COPYRIGHT[ ]AND[ ]LICENSE\n
    .+
    GNU\s+Affero\s+General\s+Public\s+License
    .+
    <https://www\.gnu\.org/licenses/>\.\n
    """,
    re.DOTALL | re.VERBOSE,
)


def should_return_exit_code_64_when_passing_no_parameters():
    result = run("./pacman-package-file.bash", check=False)
    assert result.returncode == 64


def should_print_documentation_on_standard_error_when_passing_no_parameters():
    result = run("./pacman-package-file.bash", stderr=PIPE, check=False)
    assert DOCUMENTATION_REGEX.fullmatch(result.stderr) is not None


def should_print_documentation_on_standard_output_when_passing_help_flag():
    result = run(["./pacman-package-file.bash", "--help"], stdout=PIPE, check=True)
    assert DOCUMENTATION_REGEX.fullmatch(result.stdout) is not None


def should_print_error_if_owning_package_cannot_be_found():
    with NamedTemporaryFile() as non_package_file:
        result = run(
            ["./pacman-package-file.bash", non_package_file.name],
            stderr=PIPE,
            check=False,
        )
    assert result.stderr == bytes(
        f"pacman-package-file.bash: No package owns {non_package_file.name}\n",
        encoding="utf-8",
    )


def should_return_exit_code_1_when_pacman_outputs_an_error():
    with NamedTemporaryFile() as non_package_file:
        result = run(["./pacman-package-file.bash", non_package_file.name], check=False)
    assert result.returncode == 1


def should_print_error_if_owning_package_is_ambiguous():
    result = run(["./pacman-package-file.bash", "/etc"], stderr=PIPE, check=False)
    assert result.stderr == bytes(
        "pacman-package-file.bash: Multiple packages own /etc\n",
        encoding="utf-8",
    )


def should_return_exit_code_8_when_owning_package_is_ambiguous():
    result = run(["./pacman-package-file.bash", "/etc"], check=False)
    assert result.returncode == 8


def should_print_error_if_package_file_does_not_exist():
    # Given the wrong cache directory
    environment = {"PACMAN_CONFIG": f"{DIRECTORY}/pacman.conf"}

    # When requesting a file from an installed package
    result = run(
        ["./pacman-package-file.bash", "/etc/hosts"],
        stderr=PIPE,
        check=False,
        env=environment,
    )

    # Then it should print an error message
    assert result.stderr == bytes(
        (
            "pacman-package-file.bash: Package missing -"
            " download using `pacman --sync --downloadonly filesystem` and retry\n"
        ),
        encoding="utf-8",
    )


def should_return_exit_code_9_if_package_file_does_not_exist():
    # Given the wrong cache directory
    environment = {"PACMAN_CONFIG": f"{DIRECTORY}/pacman.conf"}

    # When requesting a file from an installed package
    result = run(
        ["./pacman-package-file.bash", "/etc/hosts"], check=False, env=environment
    )

    # Then it should exit with code 9
    assert result.returncode == 9
