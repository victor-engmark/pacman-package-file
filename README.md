# pacman-package-file

[![pipeline status](https://gitlab.com/victor-engmark/pacman-package-file/badges/master/pipeline.svg)](https://gitlab.com/victor-engmark/pacman-package-file/-/commits/master) 

Get original file contents from package.

## Use

    ./pacman-package-file.bash PATH

## Example

Get the original pacman.conf file contents:

    ./pacman-package-file.bash /etc/pacman.conf

## Bugs

Please submit bugs using the [project issue tracker](https://gitlab.com/victor-engmark/pacman-package-file/-/issues).

## Development

```shell script
pre-commit install --hook-type=commit-msg --overwrite
pre-commit install --hook-type=pre-commit --overwrite
```

## License

[GNU Affero GPL v3 or later](LICENSE)

## Copyright

© 2020 Victor Engmark
